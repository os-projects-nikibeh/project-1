#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define STDIN 0
#define MSGMAXLEN 512

int main(int argc, char *argv[])
{
    int heartbeatPort, broadcastPort, clientPort;
    int clientSocket, heartbeatSocket, broadcastSocket;
    int nbytes;
    int requestedFd;
    int newFile;
    char *command;
    char *fileName;
    char* openingFileContent;
    char fileContent[2048];
    char buffer[MSGMAXLEN];
    char helper[12];
    char heartbeatMessage[12];
    struct sockaddr_in serverAddr, heartbeatAddr, broadcastAddr;
    int heartbeatAddrSize = sizeof(heartbeatAddr);
    fd_set readFds;
    struct stat sb;
    struct timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    if(argc < 4)
    {
        write(1, "Please enter heartbeat port, broadcast port & client port...\n",
            sizeof("Please enter heartbeat port, broadcast port & client port...\n"));
        exit(EXIT_FAILURE);
    }
    else if(argc > 4)
    {
        write(1, "Invalid input...\n", sizeof("Invalid input...\n"));
        exit(EXIT_FAILURE);
    }
    else
    {
        heartbeatPort = atoi(argv[1]);
        broadcastPort = atoi(argv[2]);
        clientPort = atoi(argv[3]);
    }
    
    heartbeatSocket = socket(AF_INET, SOCK_DGRAM, 0);
    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    broadcastSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if(heartbeatSocket == -1 || clientSocket == -1 || broadcastSocket == -1)
    {
        write(1, "Socket creation failed...\n", sizeof("Socket creation failed...\n"));
        exit(0);
    }
    else
        write(1, "Socket creation succeeded...\n", sizeof("Socket creation succeeded...\n"));
    
    bzero(&heartbeatAddr, sizeof(heartbeatAddr));
    heartbeatAddr.sin_family = AF_INET;
    heartbeatAddr.sin_port = htons(heartbeatPort);
    heartbeatAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if(setsockopt(heartbeatSocket, SOL_SOCKET, SO_BROADCAST, (struct timeval *) &tv, sizeof(struct timeval)) == -1)
    {
        write(1, "Setting broadcast failed...\n", sizeof("Setting broadcast failed...\n"));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(heartbeatSocket, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *) &tv, sizeof(struct timeval)) == -1)
    {
        write(1, "Setting rcvtimeo failed...\n", sizeof("Setting rcvtimeo failed...\n"));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(heartbeatSocket, SOL_SOCKET, SO_REUSEADDR, (struct timeval *) &tv, sizeof(struct timeval)) == -1)
    {
        write(1, "Setting reuseaddr failed...\n", sizeof("Setting reuseaddr failed...\n"));
        exit(EXIT_FAILURE);
    }

    if(bind(heartbeatSocket, (struct sockaddr *) &heartbeatAddr, sizeof(heartbeatAddr)) == -1)
    {
        write(1, "Socket bind failed...\n", sizeof("Socket bind failed...\n"));
        exit(EXIT_FAILURE);
    }
    else
        write(1, "Socket bind succeeded...\n", sizeof("Socket bind succeeded...\n"));

    if(recvfrom(heartbeatSocket, (char *)heartbeatMessage, 1024, 0, 
    (struct sockaddr *) &heartbeatAddr, &heartbeatAddrSize) < 0)
    {
        write(1, "Server's listening port is not recognized..\n",
            sizeof("Server's listening port is not recognized..\n"));
        exit(EXIT_FAILURE);
    }
    else
        write(1, "Server's listening port is recognized...\n", 
            sizeof("Server's listening port is recognized...\n"));
    
    bzero(&broadcastAddr, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_port = htons(broadcastPort);
    broadcastAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if(setsockopt(broadcastSocket, SOL_SOCKET, SO_BROADCAST, (struct timeval *) &tv, sizeof(struct timeval)) == -1)
    {
        write(1, "Setting broadcast failed...\n", sizeof("Setting broadcast failed...\n"));
        exit(EXIT_FAILURE);
    }

    if(setsockopt(broadcastSocket, SOL_SOCKET, SO_REUSEADDR, (struct timeval *) &tv, sizeof(struct timeval)) == -1)
    {
        write(1, "Setting reuseaddr failed...\n", sizeof("Setting reuseaddr failed...\n"));
        exit(EXIT_FAILURE);
    }


    bzero(&serverAddr, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(atoi((char *)heartbeatMessage)); //should be the Y port which is the message of heartbeat
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if(connect(clientSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) == -1)
    {
        write(1, "Connecting to the sever failed...\n", 
            sizeof("Connecting to the sever failed...\n"));
        exit(EXIT_FAILURE);
    }
    else
        write(1, "Connected to the server...\n", sizeof("Connected to the server...\n"));

    FD_ZERO(&readFds);
    FD_SET(STDIN, &readFds);
    FD_SET(heartbeatSocket, &readFds);
    FD_SET(clientSocket, &readFds);

    select(STDIN + 1, &readFds, NULL, NULL, NULL);
    if(FD_ISSET(STDIN, &readFds))
    {
        nbytes = read(STDIN, buffer, MSGMAXLEN);

        if(nbytes < 0 || nbytes == 0)
        {
            write(1, "Reading failed...\n", sizeof("Reading failed...\n"));
            exit(EXIT_FAILURE);
        }
        else
        {
            if(send(clientSocket, (char *)buffer, strlen(buffer), 0) < 0)
            {
                write(1, "Sending stdin to the server failed...\n",
                    sizeof("Sending stdin to the server failed...\n"));
                exit(EXIT_FAILURE);
            }
            
            command = strtok(buffer, " ");
            fileName = strtok(NULL, "\n");

            if(strcmp(command, "upload") == 0)
            {
                if(recv(clientSocket, helper, 12, 0) < 0)
                {
                    write(1, "Recieving helper failed..\n", 
                        sizeof("Recieving helper failed..\n"));
                    exit(EXIT_FAILURE);
                }   

                requestedFd = open(fileName, O_RDONLY);
                if(requestedFd < 0)
                {
                    write(1, "Opening the file failed...\n",
                        sizeof("Opening the file failed...\n"));
                    exit(EXIT_FAILURE);
                }
                
                if(stat(fileName, &sb) != 0)
                {
                    write(1, "File size detecting failed...\n", 
                        sizeof("File size detecting failed...\n"));
                    exit(EXIT_FAILURE);
                }

                openingFileContent = (char*) malloc(sb.st_size);
                if(read(requestedFd, openingFileContent, sb.st_size) < 0)
                {
                    write(1, "Reading the content of file failed...\n",
                        sizeof("Reading the content of file failed...\n"));
                    exit(EXIT_FAILURE);
                }

                if(send(clientSocket, (char *)openingFileContent, strlen(openingFileContent), 0) < 0)
                {
                    write(1, "Sending file to the server failed...\n",
                        sizeof("Sending file to the server failed...\n"));
                    exit(EXIT_FAILURE);
                }
   
            }

            else if(strcmp(command, "download") == 0)
            {
                if(recv(clientSocket, fileContent, 2048, 0) < 0)
                {
                    write(1, "Receiving file failed...\n", 
                        sizeof("Receiving file failed...\n"));
                    exit(EXIT_FAILURE);
                }
                if(strlen(fileContent) == 1 && strcmp(fileContent, "-") == 0)
                {
                    printf("file was not on the server\n");
                    exit(EXIT_FAILURE);
                }
                        
                newFile = open(fileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
                if(write(newFile, fileContent, strlen(fileContent)) != strlen(fileContent))
                {
                    write(1, "Creating new file failed...\n",
                        sizeof("Creating new file failed...\n"));
                    exit(EXIT_FAILURE);
                }
            
                memset(fileContent, 0, strlen(fileContent));
                
            }

            
        }
    }
    
    return 0;
}