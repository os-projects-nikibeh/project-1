#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define LISTENINGPORT_Y 9101

int main(int argc, char *argv[])
{
    int heartbeatPort;
    int serverSocket, heartbeatSocket, clientSocket;
    struct sockaddr_in serverAddr, heartbeatAddr, clientAddr;
    int heartbeatAddrSize = sizeof(heartbeatAddr);
    int clientAddrSize = sizeof(clientAddr);
    int broadcastOn = 1;
    int fdmax;
    int requestedFd;
    int newFile;
    char heartbeatMessage[12];
    char clientStdin[512];
    char *command;
    char *fileName;
    char fileContent[2048];
    char* openingFileContent;
    sprintf(heartbeatMessage, "%d", LISTENINGPORT_Y);
    bool heartIsBeating = false;
    fd_set activeFdSet, readFdSet;
    struct stat sb;
    int err;

    if(argc < 2)
    {
        write(1, "Please enter heartbeat port...\n", sizeof("Please enter heartbeat port...\n"));
        exit(EXIT_FAILURE);
    }
    else if(argc > 2)
    {
        write(1, "Invalid input...\n", sizeof("Invalid input...\n"));
        exit(EXIT_FAILURE);
    }
    else
        heartbeatPort = atoi(argv[1]);
    
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    heartbeatSocket = socket(AF_INET, SOCK_DGRAM, 0);
    if(serverSocket == -1 || heartbeatSocket == -1)
    {
        write(1, "Socket creation failed...\n", sizeof("Socket creation failed...\n"));
        exit(0);
    }
    else
        write(1, "Socket creation succeeded...\n", sizeof("Socket creation succeeded...\n"));


    bzero(&heartbeatAddr, sizeof(heartbeatAddr));
    heartbeatAddr.sin_family = AF_INET;
    heartbeatAddr.sin_port = htons(heartbeatPort);
    heartbeatAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    
    if(setsockopt(heartbeatSocket, SOL_SOCKET, SO_BROADCAST, &broadcastOn, sizeof(int)) == -1)
    {
        write(1, "Setsockopt failed...\n", sizeof("Setsockopt failed...\n"));
        exit(EXIT_FAILURE);
    }

    void heartbeatHandler(int signum)  
    {
        if(sendto(heartbeatSocket, (char *)heartbeatMessage, strlen(heartbeatMessage), 0, 
            (struct sockaddr *) &heartbeatAddr, heartbeatAddrSize) < 0)
            {
                write(1, "Server is not available...\n", sizeof("Server is not available...\n"));
                exit(EXIT_FAILURE);
            }
            
            alarm(1);
    }

    signal(SIGALRM, heartbeatHandler);
    alarm(1);

    bzero(&serverAddr, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(LISTENINGPORT_Y);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    if(bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) == -1)
    {
        write(1, "Socket bind failed...\n", sizeof("Socket bind failed...\n"));
        exit(EXIT_FAILURE);
    }
    else
        write(1, "Socket bind succeeded...\n", sizeof("Socket bind succeeded...\n"));
    
    if(listen(serverSocket, 10) != 0)
    {
        write(1, "Listening failed...\n", sizeof("Listening failed...\n"));
        exit(EXIT_FAILURE);
    }
    else
        write(1, "Server is listening...\n", sizeof("Server is listening...\n"));

    FD_ZERO(&activeFdSet);
    FD_ZERO(&readFdSet);
    FD_SET(serverSocket, &activeFdSet);
    fdmax = serverSocket;
    
    while(1)
    {   
        readFdSet = activeFdSet;
       
        if((err = select(fdmax + 1, &readFdSet, NULL, NULL, NULL)) == -1 && errno != EINTR)
        {
            write(1, "Selecting failed...\n", sizeof("Selecting failed...\n"));
            exit(EXIT_FAILURE);
        }

        if(err < 0)
            continue;   

        for(int i = 0; i <= fdmax; i++)
        {
            if(FD_ISSET(i, &readFdSet))
            {
                if(i == serverSocket)
                {
                    int newSocket;
                    newSocket = accept(serverSocket, (struct sockaddr *) &clientAddr, &clientAddrSize);
                    if(newSocket < 0)
                    {
                        write(1, "Accepting the connection failed...\n", sizeof("Accepting the connection failed...\n"));
                        exit(EXIT_FAILURE);
                    }
                    
                    write(1, "Server is connected to the client...\n", sizeof("Server is connected to the client...\n"));
                    FD_SET(newSocket, &activeFdSet);
                    if(newSocket > fdmax)
                        fdmax = newSocket;
                }
                else
                {
                    if(recv(i, clientStdin, 512, 0) < 0)
                    {
                        write(1, "Receiving clientStdin failed...\n", 
                            sizeof("Receiving clientStdin failed...\n"));
                        exit(EXIT_FAILURE);
                    }

                    command = strtok(clientStdin, " ");
                    fileName = strtok(NULL, "\n");

                    if(strcmp(command, "upload") == 0)
                    {
                        if(send(i, "helper", strlen("helper"), 0) < 0)
                        {
                            write(1, "Sending helper failed...\n", 
                                sizeof("Sending helper failed...\n"));
                            exit(EXIT_FAILURE);
                        }
                        
                        if(recv(i, fileContent, 2048, 0) < 0)
                        {
                            write(1, "Receiving file failed...\n", 
                                sizeof("Receiving file failed...\n"));
                            exit(EXIT_FAILURE);
                        }
                        
                        newFile = open(fileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
                        if(write(newFile, fileContent, strlen(fileContent)) != strlen(fileContent))
                        {
                            write(1, "Creating new file failed...\n",
                                sizeof("Creating new file failed...\n"));
                            exit(EXIT_FAILURE);
                        }

                        memset(fileContent, 0, strlen(fileContent));

                    }

                    else if(strcmp(command, "download") == 0)
                    {
                        requestedFd = open(fileName, O_RDONLY);
                        if(requestedFd < 0)
                        {
                            /*
                            write(1, "Opening the file failed...\n",
                                sizeof("Opening the file failed...\n"));
                            exit(EXIT_FAILURE);
                            */
                            write(1, "The requested file is not available on server...\n",
                                sizeof("The requested file is not available on server...\n"));
                            write(1, "Checking other clients for your request...\n",
                                sizeof("Checking other clients for your request...\n"));
                            if(send(i, "-", 1, 0) < 0)
                            {
                                write(1, "Sending <not found> message failed...\n",
                                    sizeof("Sending <not found> message failed...\n"));
                                exit(EXIT_FAILURE);
                            }
                            
                        }
                        else
                        {
                            if(stat(fileName, &sb) != 0)
                            {
                                write(1, "File size detecting failed...\n", 
                                    sizeof("File size detecting failed...\n"));
                                exit(EXIT_FAILURE);
                            }

                            openingFileContent = (char*) malloc(sb.st_size);
                            if(read(requestedFd, openingFileContent, sb.st_size) < 0)
                            {
                                write(1, "Reading the content of file failed...\n",
                                    sizeof("Reading the content of file failed...\n"));
                                exit(EXIT_FAILURE);
                            }

                            if(send(i, (char *)openingFileContent, strlen(openingFileContent), 0) < 0)
                            {
                                write(1, "Sending file to the client failed...\n",
                                    sizeof("Sending file to the client failed...\n"));
                                exit(EXIT_FAILURE);
                            }
                        }
                    }

                    if(requestedFd >= 0)
                    {
                        FD_CLR(i, &activeFdSet);
                        if(close(i) < 0)
                        {
                            write(1, "Closing connecton failed...\n",
                                sizeof("Closing connecton failed...\n"));
                            exit(EXIT_FAILURE);
                        }
                        write(1, "Request is done...\n", sizeof("Request is done...\n"));
                        write(1, "Client disconnected...\n", sizeof("Client disconnected...\n"));
                    }
                }
            }
        }
    }


    return 0;
}

